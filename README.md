# Cloudsploit CI

Cloudsploit for using inside CI or local user machines

## Run locally


```bash
$~   docker run --rm -it registry.gitlab.com/helecloud/containers/cloudsploit-ci:latest /bin/ash
```

## Run inside GitLab CI

```yaml
cloudsploit:
  stage: secure
  image:
    name: registry.gitlab.com/helecloud/containers/cloudsploit-ci:latest
    entrypoint:
      - /usr/bin/env
  script:
    - cloudsploitscan > checks/cloudsploit.json
  artifacts:
    paths:
      - checks/cloudsploit.json
```

## Maintainer

* Will Hall
